FROM python:3.12-alpine as base

ARG FLUX_LOCAL_VERSION="4.3.1"
ARG DYFF_VERSION="1.7.1"

RUN apk add --no-cache ca-certificates git curl; \
    pip install --no-cache-dir flux-local==${FLUX_LOCAL_VERSION}

RUN set -x && \
    curl -LO https://github.com/homeport/dyff/releases/download/v${DYFF_VERSION}/dyff_${DYFF_VERSION}_linux_amd64.tar.gz && \
    tar zxf dyff_${DYFF_VERSION}_linux_amd64.tar.gz && \
    rm dyff_${DYFF_VERSION}_linux_amd64.tar.gz && \
    mv dyff /usr/local/bin/dyff

COPY --from=ghcr.io/fluxcd/flux-cli:v2.2.3              /usr/local/bin/flux              /usr/local/bin/flux
COPY --from=docker.io/alpine/helm:3.14.1                /usr/bin/helm                    /usr/local/bin/helm
COPY --from=docker.io/bitnami/kubectl:1.29.2            /opt/bitnami/kubectl/bin/kubectl /usr/local/bin/kubectl
COPY --from=registry.k8s.io/kustomize/kustomize:v5.3.0  /app/kustomize                   /usr/local/bin/kustomize

USER 1001
ENTRYPOINT ["/usr/local/bin/flux-local"]